#include <iostream>

#include <QLibrary>
#include <QDirIterator>
#include <QCoreApplication>

#include <PluginManager/PluginManager.h>
#include "WidgetProviderPlugin.h"

int main(int argc, char *argv[]){
	QCoreApplication app(argc,argv);

	QDirIterator it(QCoreApplication::applicationDirPath(),{"*.dll","*.so"},QDir::Files,QDirIterator::Subdirectories);
	while (it.hasNext()) {
		it.next();
		QLibrary(it.filePath()).load();
	}

	for (auto& constructor : PluginManager::constructors) {


		if(auto plugin= std::dynamic_pointer_cast<WidgetProviderPlugin>(constructor.second()) ){
			plugin->stuff();
		}
	}

}
