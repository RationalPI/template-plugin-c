#pragma once

#include <map>
#include <functional>
#include <memory>
#include <string>
#include <iostream>
#include <sstream>

namespace JustForFun {
	void log(){
		std::cout << std::endl;
	}
	template <typename T, typename... Types>
	void log(T var1, Types... var2){
		std::cout << var1;
		log(var2...);
	}
}
#ifndef NDEBUG
# define LOG(...) JustForFun::log(__VA_ARGS__)
#else
# define LOG(...)
#endif


#ifdef WIN32
#  ifdef PLUGIN_MANAGER
#    define PLUGIN_MANAGER_EXPORT __declspec(dllexport)
#  else
#    define PLUGIN_MANAGER_EXPORT __declspec(dllimport)
#  endif
#else
#  define PLUGIN_MANAGER_EXPORT
#endif

struct Plugin{
	virtual ~Plugin(){}
};

struct PLUGIN_MANAGER_EXPORT PluginManager{
	static std::map<std::string,std::function<std::shared_ptr<Plugin>()>> constructors;

	template<class PluginDerivate>
	struct AddPlugin{
		AddPlugin(){
			LOG(PluginDerivate::ID," added to PluginManager");
			if(PluginManager::constructors.count(PluginDerivate::ID)){
				std::stringstream ss;
				ss << PluginDerivate::ID << ":doubleLoad";
				throw ss.str();
			}
			PluginManager::constructors[PluginDerivate::ID]=[](){
				return std::make_shared<PluginDerivate>();
			};
		}
		~AddPlugin(){
			LOG(PluginDerivate::ID," removed to PluginManager");
			PluginManager::constructors.erase(PluginDerivate::ID);
		}
	};
};
