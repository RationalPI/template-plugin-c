#include <PluginManager/PluginManager.h>
#include <WidgetProviderPlugin.h>
#include <iostream>

struct PluginA:public WidgetProviderPlugin{
	void stuff() override{
		LOG("PluginA::stuff() called");
	}
	static constexpr auto ID="PluginA";
};


PluginManager::AddPlugin<PluginA> _;
