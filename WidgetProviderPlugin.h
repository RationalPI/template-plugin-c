#pragma once

#include <PluginManager/PluginManager.h>

struct WidgetProviderPlugin :public Plugin{
    virtual void stuff()=0;
};
